import os
import urllib

from google.appengine.ext import blobstore
from google.appengine.ext import webapp
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from google.appengine.api import memcache

class QHImage(db.Model):
	filekey = db.StringProperty()
	name = db.StringProperty()


class MainHandler(webapp.RequestHandler):
	def get(self):
		self.redirect('http://www.qhoach.com/')

class OpenHandler(webapp.RequestHandler):
	def get(self):
		upload_url = blobstore.create_upload_url('/upload')
		self.response.out.write(upload_url)

class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
	def post(self):
		upload_files = self.get_uploads('file')  # 'file' is file upload field in the form
		blob_info = upload_files[0]
		
		filename = self.request.get('zoom') + "_" + self.request.get('lat') + "_" + self.request.get('lon')
		
		img_info = QHImage()
		img_info.filekey = str(blob_info.key())
		img_info.name = filename
		img_info.put()
		
		self.redirect('/test/%s' % img_info.name)

class TileHandler(blobstore_handlers.BlobstoreDownloadHandler):
	def get(self, resource):
		resource = str(urllib.unquote(resource))
		data = memcache.get(resource)
		if data is not None:
			self.send_blob(data)
		else :
			trig = 0
			img_info = db.GqlQuery("SELECT * FROM QHImage WHERE name = '%s'" % resource)
			for im in img_info:
				trig = 1
				blob_info = blobstore.BlobInfo.get(img_info[0].filekey)
				self.send_blob(blob_info)
				memcache.add(resource, blob_info, 86400) # cache in 1 day
			if trig == 0:
				self.redirect('/static/none.png')

class TestHandler(blobstore_handlers.BlobstoreUploadHandler):
	def get(self, resource):
		resource = str(urllib.unquote(resource))
		self.response.out.write('Uploaded to %s' % resource)

class CheckHandler(webapp.RequestHandler):
	def get(self):
		offset = int(self.request.get("offset"))
		limit = int(self.request.get("limit"))
		rpc = db.create_rpc(deadline=50, read_policy=db.STRONG_CONSISTENCY)
		query = QHImage.all().fetch(limit=limit, offset=offset, rpc=rpc)
		for result in query:
			self.response.out.write("|%s" % result.name)

def main():
	application = webapp.WSGIApplication(
					[('/', MainHandler),
					('/upload', UploadHandler),
					('/sfxKDA', OpenHandler),
					('/hdjeHDt', CheckHandler),
					('/tiles/([^/]+)?', TileHandler),
					('/test/([^/]+)?', TestHandler),
					], debug=True)
	run_wsgi_app(application)

if __name__ == '__main__':
	main()